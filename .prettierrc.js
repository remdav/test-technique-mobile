module.exports = {
  semicolons: true,
  bracketSpacing: false,
  jsxBracketSameLine: true,
  singleQuote: true,
  trailingComma: "none",
  printWidth: 120,
  jsxSingleQuote: true,
  endOfLine: "auto",
};
