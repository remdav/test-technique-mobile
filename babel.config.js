module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: ['.ts', '.tsx', '.jsx', '.js'],
        alias: {
          navigators: './src/navigators',
          application: './src/application',
          style: './src/style',
          images: './src/images',
          types: './src/types',
          managers: './src/managers'
        }
      }
    ]
  ]
};
