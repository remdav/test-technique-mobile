import {useNavigation} from '@react-navigation/native';
import {infoWeather} from 'managers';
import React, {FunctionComponent, useState} from 'react';
import {Alert, Image, SafeAreaView, Text, TextInput, TouchableOpacity, View} from 'react-native';
import {ChoiceCity as styles} from 'style/application';
import {weatermap} from 'types/weatermap';

const ChoiceCity: FunctionComponent = () => {
  const [city, setCity] = useState<string>('');
  const navigate = useNavigation();

  const changeCity = (contentText: string): void => {
    setCity(contentText);
  };

  const changeView = async (): Promise<void> => {
    let isCityExist: undefined | weatermap;

    try {
      isCityExist = await infoWeather(city);
    } catch (error) {
      Alert.alert('Oupsss...', "Cette ville n'existe pas");
      return;
    }

    navigate.navigate('Temperature', {
      city
    });

    setCity('');
  };

  return (
    <SafeAreaView>
      <Image style={styles.image} source={require('images/Home/avena-event.png')} />
      <View style={styles.content}>
        <View style={styles.contentTextInput}>
          <Text style={styles.labelTextInput}>Quel temps fait-il :</Text>
          <TextInput
            style={styles.textInput}
            placeholderTextColor={'white'}
            placeholder={"Tapez le nom d'une ville"}
            onChangeText={changeCity}
            value={city}
          />
        </View>
        <TouchableOpacity style={styles.button} onPress={changeView}>
          <Text>Valider</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

export default ChoiceCity;
