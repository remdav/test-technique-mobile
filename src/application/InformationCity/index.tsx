import {RouteProp, useRoute} from '@react-navigation/native';
import {HotOrCold} from 'components';
import React, {FunctionComponent} from 'react';

type InformationCityScreenRouteProp = RouteProp<{Temperature: {city: string}}, 'Temperature'>;

const InformationCity: FunctionComponent = () => {
  const route = useRoute<InformationCityScreenRouteProp>();

  return <HotOrCold city={route.params.city} />;
};

export default InformationCity;
