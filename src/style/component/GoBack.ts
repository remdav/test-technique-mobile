import {StyleSheet} from 'react-native';

const GoBack = StyleSheet.create({
  button: {
    position: 'absolute',
    top: '5%',
    left: 20,
    zIndex: 1,
    padding: 20
  },
});

export default GoBack;
