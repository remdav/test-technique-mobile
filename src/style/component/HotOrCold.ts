import {StyleSheet} from 'react-native';

const HotOrCold = StyleSheet.create({
  content: {
    position: 'relative'
  },
  view: {
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative'
  },
  text: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold'
  },
});

export default HotOrCold;
