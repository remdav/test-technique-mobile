const theme = {
  primary: '#93127e',
  hot: '#ff8a3d',
  cold: '#7fd6ff'
}

export default theme;
