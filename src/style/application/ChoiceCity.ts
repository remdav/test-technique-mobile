import {StyleSheet} from 'react-native';

const ChoiceCity = StyleSheet.create({
  image: {
    alignSelf: 'center',
    marginTop: 30
  },
  content: {
    alignItems: 'center',
    height: '80%',
    justifyContent: 'center'
  },
  contentTextInput: {
    width: '60%'
  },
  labelTextInput: {
    marginBottom: 20,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20
  },
  textInput: {
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    marginBottom: 20,
    color: 'white'
  },
  button: {
    backgroundColor: 'white',
    paddingHorizontal: 30,
    paddingVertical: 10,
    borderRadius: 20
  }
});

export default ChoiceCity;
