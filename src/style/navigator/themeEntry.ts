import {DefaultTheme} from '@react-navigation/native';
import {theme} from 'style';

const {primary} = theme;

const themeEntry = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: primary,
    color: 'white'
  }
};

export default themeEntry;
