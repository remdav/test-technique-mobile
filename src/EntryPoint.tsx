import Navigator from 'navigators';
import React from 'react';

const EntryPoint = () => {
  return <Navigator />;
};

export default EntryPoint;
