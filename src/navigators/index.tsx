import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {ChoiceCity, InformationCity} from 'application';
import React, {FunctionComponent} from 'react';
import {themeEntry} from 'style/navigator';

const Stack = createStackNavigator();

const Entry: FunctionComponent = () => {
  return (
    <NavigationContainer theme={themeEntry}>
      <Stack.Navigator headerMode={'none'}>
        <Stack.Screen name='ChoiceCity' component={ChoiceCity} options={{title: 'Choix de la ville'}} />
        <Stack.Screen name='Temperature' component={InformationCity} options={{title: 'température'}} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Entry;
