import {GoBack} from 'components';
import {infoWeather} from 'managers';
import React, {FunctionComponent, useEffect, useState} from 'react';
import {Alert, SafeAreaView, Text, View} from 'react-native';
import {theme} from 'style';
import {HotOrCold as styles} from 'style/component';
import {weatermap} from 'types/weatermap';

interface HotOrColdProps {
  city: string;
}

const HotOrCold: FunctionComponent<HotOrColdProps> = ({city}) => {
  const [information, setInformation] = useState<{color: string; text: string}>({color: '', text: ''});

  const getInfoPage = async (): Promise<void> => {
    let infoCity: undefined | weatermap;

    try {
      infoCity = await infoWeather(city);
    } catch (error) {
      Alert.alert('Oupsss...', "Une erreur s'est produite");
      return;
    }

    const {temp} = infoCity.main;

    if (temp < 15) {
      setInformation({color: theme.cold, text: 'Il fait froid'});
      return;
    }

    setInformation({color: theme.hot, text: 'Il fait chaud'});
  };

  useEffect(() => {
    getInfoPage();

    () => getInfoPage();
  }, []);

  return (
    <View style={styles.content}>
      <GoBack />
      {information.color !== '' && (
        <SafeAreaView style={[{backgroundColor: information.color}, styles.view]}>
          <Text style={styles.text}>{information.text}</Text>
        </SafeAreaView>
      )}
    </View>
  );
};

export default HotOrCold;
