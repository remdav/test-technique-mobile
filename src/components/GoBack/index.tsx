import {useNavigation} from '@react-navigation/native';
import React, {FunctionComponent} from 'react';
import {TouchableOpacity} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {GoBack as styles} from 'style/component';

const GoBack: FunctionComponent = () => {
  const navigation = useNavigation();

  const goBack = () => {
    navigation.goBack();
  };

  return (
    <TouchableOpacity style={styles.button} onPress={goBack}>
      <FontAwesome5 name={'chevron-left'} size={20} />
    </TouchableOpacity>
  );
};

export default GoBack;
