import axios from 'axios';
import {weatermap} from 'types/weatermap';

const infoWeather = async (city: string): Promise<weatermap> => {
  const tokenApi = '';

  const weatherCity = await axios.get(
    `https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${tokenApi}&mode=json&units=metric`
  );

  return weatherCity.data;
};

export default infoWeather;
